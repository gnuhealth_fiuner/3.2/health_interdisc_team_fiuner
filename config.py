# -*- coding: utf-8 -*-
##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from trytond.model import ModelView, ModelSingleton, ModelSQL, fields, Unique
from trytond.pool import Pool, PoolMeta

__all__ = [
    'GnuHealthSequences'
    ]

class GnuHealthSequences(metaclass = PoolMeta):
    'Standard Sequences for GNU Health'
    __name__ = 'gnuhealth.sequences'
    
    healthTeamAppointment_sequence = fields.Property(fields.Many2One(
        'ir.sequence', 'Health Team Appointment Sequence', required=True,
        domain=[('code', '=', 'gnuhealth.health.team.app')]))
    
